/*
 * main.cpp
 *
 *  Created on: Mar 16, 2018
 *      Author: PCM678
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Let's play a game called "I Dunno Where to Makan So You Guys Decide Lah~"
 * Rules:
 * 1. Everyone list down how many candidate places to makan and their number.
 * 2. Number of total candidates is #define total_selections.
 * 3. Everyone give one random number and put into their own respective #defines
 * 4. Run program
 * 5. PROFIT.
 */
#define pat 1
#define hp  1
#define yn  1
#define js  1
#define rn  1
#define mh  1
#define wy  1
#define hs  1
#define gn  1
#define total_selections 2

int sum_till_single_digit(int arg_raw)
{
    int local_ret_result = arg_raw;
    bool local_first_time = true;

    do
    {
        if (local_first_time)
        {
            // Do nothing, no need to add random number yet
        }
        else
        {
            srand(time(NULL));
            local_ret_result = local_ret_result + rand()%total_selections;
        }

        while(local_ret_result/10)
        {
            local_ret_result = (local_ret_result/10) + (local_ret_result%10);
        }

        local_first_time = false;
    }while(local_ret_result > total_selections);

    return local_ret_result;
}

int sum_till_double_digit(int arg_raw)
{
    int local_ret_result = arg_raw;

    while(local_ret_result > total_selections)
    {
        local_ret_result = (local_ret_result/100) + (local_ret_result%10);
    }

    return local_ret_result;
}

int main(void)
{
    int local_raw = pat + hp + yn + js + rn + mh + wy + hs + gn;
    int local_target_numbers = total_selections;
    bool local_raw_is_single_digit = true;
    bool local_target_numbers_is_single_digit = true;

    if (local_raw/10)
    {
        local_raw_is_single_digit = false;
    }
    else
    {
        local_raw_is_single_digit = true;
    }

    if (local_target_numbers/10)
    {
        local_target_numbers_is_single_digit = false;
    }
    else
    {
        local_target_numbers_is_single_digit = true;
    }

    if (local_raw == local_target_numbers)
    {
        // GOOD! But I'm a bad person, so...
        srand(time(NULL));
        int local_iterations = rand()%local_target_numbers;

        for (int iterations = 0; iterations<=local_iterations; iterations++)
        {
            srand(time(NULL));
            local_raw = local_raw + (rand()%local_target_numbers);
        }

        if (local_target_numbers_is_single_digit)
        {
            printf("BINGO! Selected: %d\n", sum_till_single_digit(local_raw));
        }
        else
        {
            printf("BINGO! Selected: %d\n", sum_till_double_digit(local_raw));
        }
    }
    else if (local_raw > local_target_numbers)
    {
        if (local_target_numbers_is_single_digit)
        {
            printf("BINGO! Selected: %d\n", sum_till_single_digit(local_raw));
        }
        else
        {
            printf("BINGO! Selected: %d\n", sum_till_double_digit(local_raw));
        }
    }
    else if (local_raw < local_target_numbers)
    {
        // GOOD! But I'm a bad person, so...
        srand(time(NULL));
        int local_iterations = rand()%local_target_numbers;

        for (int iterations = 0; iterations<=local_iterations; iterations++)
        {
            srand(time(NULL));
            local_raw = local_raw + (rand()%local_target_numbers);
        }

        if (local_target_numbers_is_single_digit)
        {
            printf("BINGO! Selected: %d\n", sum_till_single_digit(local_raw));
        }
        else
        {
            printf("BINGO! Selected: %d\n", sum_till_double_digit(local_raw));
        }
    }
    else
    {
        // Exception case, do nothing
        printf("Shit, kena exception case pulak...\n");
    }

    return 0;
}
